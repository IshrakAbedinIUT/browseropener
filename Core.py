from pyvirtualdisplay import Display
import webbrowser
import enum

def isTriggerOn(toggleTrigger : bool) -> bool:
    try:
        triggerFile = open("./Trigger.txt", "r")
        triggerVal =(int)(triggerFile.readline())
        triggerFile.close()
        if triggerVal > 0:

            if toggleTrigger:
                isSuccessful = postTriggerTurnOff()
            else:
                isSuccessful = True

            if isSuccessful:
                return True
            else:
                return False

        else:
            return False
    except:
        return False

def postTriggerTurnOff() -> bool:
    try:
        triggerFile = open("./Trigger.txt", "w")
        triggerFile.write("0")
        triggerFile.close()
        return True
    except:
        return False

def readLink() -> list:
    linkList = []
    try:
        linkFile = open("Links.txt", "r")
        links = linkFile.readlines()
        linkFile.close()
        for line in links:
            if "$|" in line:
                link = (str)(line.split("|")[-1])
                linkList.append(link)
        return linkList
    except:
        return linkList
    
def main():
    if(isTriggerOn(False)):
        print("Trigger is on")
        links = readLink()
        for link in links:
            try:
                display = Display(visible=1)
                display.start()
                webbrowser.open(link)
                display.wait()
                display.stop()
            except:
                print("Failed opening", link)

main()